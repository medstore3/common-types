<img src="https://pbs.twimg.com/profile_images/1285630920263966721/Uk6O1QGC_400x400.jpg" align="right" width="50px">

<br><br>

# Medstore Common Types - Pacote NPM

Este é um arquivo README para a biblioteca TypeScript desenvolvida para armazenar tipagens comuns entre os sistemas que formam o Medstore. Essa biblioteca tem como objetivo fornecer um conjunto de tipos reutilizáveis que podem ser compartilhados entre diferentes microserviços em um ambiente de desenvolvimento baseado em TypeScript.

## Visão Geral

Os microserviços são uma arquitetura popular para desenvolver aplicativos escaláveis e modulares. No entanto, muitas vezes é necessário compartilhar definições de tipos entre diferentes serviços para garantir a consistência e a integração corretas dos dados.

Essa biblioteca fornece uma solução para esse problema, oferecendo um conjunto centralizado de definições de tipos que podem ser compartilhadas e reutilizadas em vários microserviços.

## Características

- **Tipagens Comuns**: A biblioteca contém uma variedade de tipos de entrada e saída usados no Medstore.

- **Reutilizável**: As tipagens são projetadas para serem reutilizáveis, e facilitar a integração entre os microserviços.

- **Manutenção Centralizada**: Ao armazenar as tipagens comuns em uma biblioteca centralizada, é mais fácil manter e atualizar as definições de tipos em todos os microserviços que a utilizam. Isso garante a consistência e evita a duplicação desnecessária de código.

## Ambiente de Desenvolvimento

### Pré-requisitos

Certifique-se de ter as seguintes ferramentas instaladas em sua máquina de desenvolvimento:

- Docker
- Docker Compose

### Passo a Passo

1. Clone este repositório em sua máquina local:

```bash
git clone <url_do_repositorio>
```

2. Acesse o diretório raiz do projeto:

```bash
cd <pasta_do_projeto>
```

3. Abra com o Visual Studio Code:

```bash
code .
```

4. Clique no botão: Abrir em um container:

<img src="https://code.visualstudio.com/assets/docs/devcontainers/create-dev-container/dev-container-reopen-prompt.png">

## Instalação

Para instalar esta biblioteca em um projeto, você pode usar o gerenciador de pacotes npm. Execute o seguinte comando no diretório raiz do seu projeto:

```bash
npm install @medstore/common-types
```

## Contribuição

Contribuições são bem-vindas! Se você quiser adicionar novas tipagens ou melhorar as existentes, sinta-se à vontade para enviar um pull request para o repositório da biblioteca no GitLab. Certifique-se de seguir as diretrizes de contribuição e de teste fornecidas no projeto.

## Licença

Esta biblioteca está licenciada sob a **Licença MIT**. Sinta-se à vontade para usá-la em seus projetos comerciais ou pessoais.

<br>

<img src="https://pbs.twimg.com/profile_images/1285630920263966721/Uk6O1QGC_400x400.jpg" width="50px">
