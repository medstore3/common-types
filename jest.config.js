module.exports = {
  collectCoverageFrom: ["<rootDir>/src/**/*.ts", "!<rootDir>/src/config/**"],
  roots: ["<rootDir>/__tests__"],
  transform: {
    ".+\\.ts$": "ts-jest",
  },
  reporters: ["default", ["jest-junit", {}]],
  coverageReporters: ["json-summary", "text"],
  moduleNameMapper: {
    "^@prisma-client": "<rootDir>/prisma-client/index",
    "@/test/(.*)": "<rootDir>/__tests__/$1",
    "@/(.*)": "<rootDir>/src/$1",
    "^@antecipag-libs/blacklist$": "<rootDir>/lib/index.js",
  },
  modulePaths: ["<rootDir>"],
  setupFiles: ["<rootDir>/.jest/env.js"],
};
