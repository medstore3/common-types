import { GENERAL } from "./general.types";

export namespace API_DATA {
  export type DataToCalculate = {
    height: number;
    weight: number;
    age: number;
    gender: GENERAL.Gender;
  };

  export type CalculatedValue = {
    name: string;
    value: number;
  };
}
