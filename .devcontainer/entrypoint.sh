#!/bin/bash

cd /usr/app;

find /usr/app/node_modules -maxdepth 0 -empty -exec npm install \;

npm run build:js -- -w &
npm run build:types -- -w &
npm run copyPackageJson

while sleep 1000; do :; done;
